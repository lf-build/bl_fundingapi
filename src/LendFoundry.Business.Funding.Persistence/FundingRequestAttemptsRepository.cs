﻿using LendFoundry.Business.Funding.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Foundation.Date;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Business.Funding
{
    public class FundingRequestAttemptsRepository : MongoRepository<IApplicationFundingRequestAttempts, ApplicationFundingRequestAttempts>, IFundingRequestAttemptsRepository
    {
        static FundingRequestAttemptsRepository()
        {

            BsonClassMap.RegisterClassMap<ApplicationFundingRequestAttempts>(map =>
            {
                map.AutoMap();
                //  map.MapProperty(m => m.ReturnDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                //  map.MapProperty(m => m.VendorResponseDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(ApplicationFundingRequestAttempts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<FundingRequestAttempts>(map =>
            {
                map.AutoMap();
                var type = typeof(FundingRequestAttempts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public FundingRequestAttemptsRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "ApplicationFundingAttempts")
        {
            CreateIndexIfNotExists("ApplicationFundingAttempts_ApplicationFundingAttemptsId", Builders<IApplicationFundingRequestAttempts>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<IApplicationFundingRequestAttempts> UpdateStatus(string referenceId, IUpdateFundingAttamptStatusRequest updateRequest)
        {
            var record = Query.Where(i => i.ReferenceId == referenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {referenceId} is not found");
            var attamptId = record.OrderByDescending(f => f.AttemptDate).First().FundingRequestAttemptId;
            await Collection.UpdateOneAsync(Builders<IApplicationFundingRequestAttempts>.Filter.Where((a => a.TenantId == TenantService.Current.Id && a.FundingRequestAttemptId == attamptId)),
            Builders<IApplicationFundingRequestAttempts>.Update.Set(a => a.VendorResponseStatus, updateRequest.Status)
            .Set(a => a.VendorResponseDate, new TimeBucket(updateRequest.ResponseDate))
            .Set(a => a.ReturnCode, updateRequest.ReturnCode)
            .Set(a => a.ReturnReason, updateRequest.ReturnReason));

            record.First().VendorResponseStatus = updateRequest.Status;
            record.First().VendorResponseDate = new TimeBucket(updateRequest.ResponseDate);
            record.First().ReturnCode = updateRequest.ReturnCode;
            return record.First();
        }

        public async Task<List<IApplicationFundingRequestAttempts>> GetAttemptData(string referenceId)
        {
            var records = await Query.Where(i => i.ReferenceId == referenceId).ToListAsync();
            if (records == null || !records.Any())
                throw new NotFoundException($"Record with reference number {referenceId} is not found");
            return records.ToList();
        }

        private FilterDefinition<IApplicationFundingRequestAttempts> GetByApplicationNumberFilter(string referenceId)
        {
            return Builders<IApplicationFundingRequestAttempts>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                          && a.ReferenceId == referenceId);
        }
    }
}