﻿using LendFoundry.Business.Funding.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Funding
{
    public class FundingRepository : MongoRepository<IApplicationFunding, ApplicationFunding>, IFundingRepository
    {
        static FundingRepository()
        {
            BsonClassMap.RegisterClassMap<ApplicationFunding>(map =>
            {
                map.AutoMap();
                //map.MapProperty(p => p).SetIgnoreIfDefault(true);
                // map.MapProperty(p => p.PurposeOfLoan).SetSerializer(new EnumSerializer<PurposeOfLoan>(BsonType.String));
                // map.MapProperty(p => p.ResidenceType).SetSerializer(new EnumSerializer<ResidenceType>(BsonType.String));
                //   map.MapProperty(m => m.RequestedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(ApplicationFunding);

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<FundingRequest>(map =>
            {
                map.AutoMap();
                var type = typeof(FundingRequest);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public FundingRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "ApplicationFunding")
        {
            CreateIndexIfNotExists("ApplicationFunding_ApplicationFundingId", Builders<IApplicationFunding>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<IEnumerable<IApplicationFunding>> GetFundingRequest(IEnumerable<string> statuses)
        {
            return await Query.Where(x => statuses.Contains(x.RequestStatus)).ToListAsync();
        }

        public async Task<IApplicationFunding> GetFundingRequest(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityType == entityType);
        }

        public async Task<IApplicationFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus updateRequest)
        {
            var record = Query.Where(i => i.ReferenceId == updateRequest.ReferenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {updateRequest.ReferenceId} is not found");

            await Collection.UpdateOneAsync(Builders<IApplicationFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ReferenceId == updateRequest.ReferenceId),
            Builders<IApplicationFunding>.Update.Set(a => a.RequestStatus, updateRequest.status));
            record.First().RequestStatus = updateRequest.status;

            return record.First();
        }

        public async Task<IApplicationFunding> UpdateApplicationFunding(IApplicationFunding updateRequest)
        {

            var record = Query.FirstOrDefault(i => i.Id == updateRequest.Id);
            if (record == null)
                throw new NotFoundException($"Record with funding id {updateRequest.Id} is not found");

            record.AmountFunded = updateRequest.AmountFunded;
            record.BankAccountNumber = updateRequest.BankAccountNumber;
            record.BankAccountType = updateRequest.BankAccountType;
            record.BankRTN = updateRequest.BankRTN;
            record.BorrowersName = updateRequest.BorrowersName;
            record.EntityId = updateRequest.EntityId;
            record.EntityType = updateRequest.EntityType;
            record.FundingType = updateRequest.FundingType;
            record.LoanApplicationNumber = updateRequest.LoanApplicationNumber;
            record.ReferenceApplicationFundingId = updateRequest.ReferenceApplicationFundingId;
            record.ReferenceId = updateRequest.ReferenceId;
            record.RequestedDate = updateRequest.RequestedDate;
            record.RequestStatus = updateRequest.RequestStatus;

            await Collection.ReplaceOneAsync(Builders<IApplicationFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.Id == updateRequest.Id), record);

            return record;
        }
    }
}