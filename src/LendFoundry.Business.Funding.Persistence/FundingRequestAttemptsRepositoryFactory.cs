﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Business.Funding.Persistence
{
    public class FundingRequestAttemptsRepositoryFactory : IFundingRequestAttemptsRepositoryFactory
    {
        #region Constructor
        public FundingRequestAttemptsRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }
        #endregion

        #region Public Methods
        public IFundingRequestAttemptsRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory> ();
            var configuration = mongoConfigurationFactory.Get (reader, Provider.GetService<ILogger> ());
            return new FundingRequestAttemptsRepository(tenantService, configuration);
        }
        #endregion
    }
}
