﻿using LendFoundry.Business.Funding;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Configuration;
using LendFoundry.NumberGenerator.Client;

namespace LendFoundry.Business.Funding
{
    public class FundingServiceFactory : IFundingServiceFactory
    {

        public FundingServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
            
        public IFundingService Create(ITokenReader reader, ILogger logger)
        {

            var fundingRepositoryFactory = Provider.GetService<IFundingRepositoryFactory>();
            var fundingRepository = fundingRepositoryFactory.Create(reader);

            var fundingRequestAttemptsRepositoryFactory = Provider.GetService<IFundingRequestAttemptsRepositoryFactory>();
            var fundingRequestAttemptsRepository = fundingRequestAttemptsRepositoryFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTimeService = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var numberGeneratorServiceFactory = Provider.GetService<INumberGeneratorServiceFactory>();
            var numberGeneratorService = numberGeneratorServiceFactory.Create(reader);

            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var loggerService = loggerFactory.CreateLogger();

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);

            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            return new FundingService(fundingRepository, numberGeneratorService,logger, lookUpService, eventHubService, fundingRequestAttemptsRepository, tenantTimeService);
        }
    }
}

