﻿using LendFoundry.Business.Funding.Persistence;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Lookup;
using System.Linq;
using System.Text.RegularExpressions;

namespace LendFoundry.Business.Funding
{
    public class FundingService : IFundingService
    {
        #region Constructor

        public FundingService
      (
           IFundingRepository fundingRepository,
           IGeneratorService fundingNumberGenerator,
           ILogger logger,
           ILookupService lookupService,
           IEventHubClient eventHubClient,
           IFundingRequestAttemptsRepository fundingRequestAttemptsRepository,
          ITenantTime tenantTime
      )
        {
            FundingRepository = fundingRepository;
            FundingNumberGenerator = fundingNumberGenerator;
            FundingRequestAttemptsRepository = fundingRequestAttemptsRepository;
            EventHubClient = eventHubClient;
            CommandExecutor = new CommandExecutor(logger);
            TenantTime = tenantTime;
            Logger = logger;
            LookupService = lookupService;
        }

        #endregion Constructor

        #region Private Properties

        private CommandExecutor CommandExecutor { get; }
        private IEventHubClient EventHubClient { get; }
        private FundingConfiguration FundingConfigurations { get; }
        private IGeneratorService FundingNumberGenerator { get; }
        private IFundingRepository FundingRepository { get; }
        private IFundingRequestAttemptsRepository FundingRequestAttemptsRepository { get; }
        private ILookupService LookupService { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }
        #endregion Private Properties

        #region Public Methods

        public async Task<IApplicationFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.BankAccountNumber))
                throw new ArgumentNullException($"Bank account number is mandatory");

            if (string.IsNullOrWhiteSpace(request.BankRTN))
                throw new ArgumentNullException($"bank RTN is mandatory");

            if (string.IsNullOrWhiteSpace(request.BorrowersName))
                throw new ArgumentNullException($"borrower name is mandatory");

            if (request.AmountFunded <= 0)
                throw new ArgumentNullException($"Funding amount should be greater than zero");

            if (!Regex.IsMatch(request.BankAccountNumber, "^[a-zA-Z0-9]+$"))
                throw new InvalidArgumentException($"Invalid bank account number");

            if (!Regex.IsMatch(request.BankRTN, "^[0-9]*$"))
                throw new InvalidArgumentException($"Invalid bank RTN");

            if (!Regex.IsMatch(request.BorrowersName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid borrower name");

            IApplicationFunding applicationFunding = new ApplicationFunding(request);
            try
            {
                Logger.Info("Started Execution for AddFundingRequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service:Funding");
                await Task.Run(() =>
                {
                    var AddFundingRequestCommand = new Command
                    (
                        execute: () =>
                        {
                            applicationFunding.EntityId = entityId;
                            applicationFunding.EntityType = entityType;
                            applicationFunding.LoanApplicationNumber = entityId;
                            applicationFunding.ReferenceId = FundingNumberGenerator.TakeNext("fedchex").Result;
                            applicationFunding.RequestStatus = string.IsNullOrWhiteSpace(request.status) ? "notsent" : request.status;//LookupService.GetLookupEntry("requestStatus", "notsent").FirstOrDefault().Key;  //"not sent";
                            FundingRepository.Add(applicationFunding);
                        },
                        rollback: () => FundingRepository.Remove(applicationFunding)
                    );
                    CommandExecutor.Execute(new List<Command>() { AddFundingRequestCommand });
                });
                await EventHubClient.Publish(new ApplicationFundingAdded
                {
                    EntityId = applicationFunding.EntityId,
                    EntityType = applicationFunding.EntityType,
                    ApplicationFunding = applicationFunding
                });
                Logger.Info("Completed Execution for AddFundingRequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "  Service:Funding");

                return applicationFunding;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddFundingRequest Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service:Funding" + "Exception" + exception.Message);

                await EventHubClient.Publish(new ApplicationFundingAddFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    SyndicationName = "business-Funding"
                });
                throw new Exception(exception.Message);
            }
        }

        public async Task<IApplicationFundingRequestAttempts> AddFundingRequestAttempts(string entityType, string entityId, IFundingRequestAttempts request)
        {

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            IApplicationFundingRequestAttempts applicationfundingattempt = new ApplicationFundingRequestAttempts(request);
            try
            {
                Logger.Info("Started Execution for AddFundingRequestAttempts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service:Funding");

                await Task.Run(() =>
                {
                    var AddFundingRequestCommand = new Command
                    (
                        execute: () =>
                        {
                            applicationfundingattempt.EntityId = entityId;
                            applicationfundingattempt.EntityType = entityType;
                            applicationfundingattempt.LoanNumber = entityId;
                            applicationfundingattempt.FundingRequestAttemptId = entityId + "_" + TenantTime.Now.DateTime.ToString("yyyyMMddHHmmss");
                            applicationfundingattempt.OutboundRef = request.OutboundRef;
                            applicationfundingattempt.ReturnCode = request.ReturnCode;
                            applicationfundingattempt.ReturnDate = new TimeBucket(request.ReturnDate);
                            applicationfundingattempt.VendorResponseDate = new TimeBucket(request.VendorResponseDate);
                            applicationfundingattempt.VendorResponseStatus = request.VendorResponseStatus;
                            applicationfundingattempt.ReferenceId = request.ReferenceId;
                            FundingRequestAttemptsRepository.Add(applicationfundingattempt);
                        },
                            rollback: () => FundingRequestAttemptsRepository.Remove(applicationfundingattempt)
                        );
                    CommandExecutor.Execute(new List<Command>() { AddFundingRequestCommand });
                });
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddFundingRequestAttempts Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service:Funding" + "Exception" + exception.Message);

            }

            Logger.Info("Completed Execution for AddFundingRequestAttempts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "  Service:Funding");
            return applicationfundingattempt;
        }

        public async Task<IEnumerable<IApplicationFunding>> GetFundingRequest(List<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            try
            {
                return await FundingRepository.GetFundingRequest(statuses);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetFundingRequest Date & Time:" + TenantTime.Now + "Service:Funding" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IApplicationFunding> GetFundingData(string entityType, string entityId)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for GetFundingData Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service:Funding");

                var fundingData = await FundingRepository.GetFundingRequest(entityType, entityId);
                if (fundingData == null)
                {
                    Logger.Info($"No funding data found for {entityType}-{entityId}");
                }
                Logger.Info("Completed Execution for GetFundingData Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "  Service:Funding");
                return fundingData;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetFundingData Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service:Funding" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IApplicationFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus updaterequest)
        {
            if (updaterequest.status == null || !updaterequest.status.Any())
                throw new ArgumentException($"{nameof(updaterequest.status)} is mandatory");

            if (entityId == null || !entityId.Any())
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (string.IsNullOrWhiteSpace(updaterequest.ReferenceId))
                throw new ArgumentException($"{nameof(updaterequest.ReferenceId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for UpdateFundingStatus Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service:Funding");

                var result = await FundingRepository.UpdateFundingStatus(entityType, entityId, updaterequest);

                //Note :Import/Export task called this function where system do not have entityId. 
                //so for event used "result.EntityId"  
                await EventHubClient.Publish(new ApplicationFundingStatusUpdated
                {
                    EntityType = entityType,
                    EntityId = result.EntityId,
                    Response = result,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                if (result.RequestStatus == "confirmed")
                {
                    var fundingData = await FundingRepository.GetFundingRequest(entityType, result.EntityId);
                    await EventHubClient.Publish("ApplicationFunded", new
                    {
                        EntityType = entityType,
                        EntityId = result.EntityId,
                        Response = fundingData,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                Logger.Info("Completed Execution for UpdateFundingStatus Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "  Service:Funding");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateFundingStatus Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service:Funding" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IApplicationFundingRequestAttempts> UpdateAttamptStatus(string referenceId, IUpdateFundingAttamptStatusRequest updateRequest)
        {
            if (string.IsNullOrWhiteSpace(referenceId))
                throw new ArgumentException($"{nameof(referenceId)} is mandatory");
            try
            {
                if (!string.IsNullOrEmpty(updateRequest.ReturnCode))
                {
                    var ReturnReason = LookupService.GetLookupEntry("returnStatusCode", updateRequest.ReturnCode);
                    if (ReturnReason != null && ReturnReason.Count > 0)
                    {
                        updateRequest.ReturnReason = ReturnReason.FirstOrDefault().Value;
                    }
                }
                return await FundingRequestAttemptsRepository.UpdateStatus(referenceId, updateRequest);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateAttamptStatus ReferenceId : " + referenceId + " at :" + TenantTime.Now + "Service:Funding" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IApplicationFunding> UpdateFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.ApplicationFundingId))
                throw new ArgumentNullException("Application funding id is required");

            var applicationfundingRecord = await FundingRepository.Get(request.ApplicationFundingId);

            if (applicationfundingRecord == null)
                throw new NotFoundException($"Funding detail with id {applicationfundingRecord.Id} is not found");

            IApplicationFunding applicationfunding = new ApplicationFunding(request);
            string oldStatus = applicationfunding.RequestStatus;

            try
            {
                Logger.Info("Started Execution for UpdateFundingRequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service:Funding");

                if (!string.IsNullOrWhiteSpace(request.status))
                {
                    var status = LookupService.GetLookupEntry("requestStatus", request.status);
                    if (status == null)
                        throw new InvalidArgumentException("Status is not valid");
                }
                await Task.Run(() =>
                {
                    var AddFundingRequestCommand = new Command
                    (
                        execute: () =>
                        {
                            applicationfunding.EntityId = entityId;
                            applicationfunding.EntityType = entityType;
                            applicationfunding.LoanApplicationNumber = entityId;
                            applicationfunding.Id = request.ApplicationFundingId;
                            applicationfunding.RequestStatus = LookupService.GetLookupEntry("requestStatus", request.status).FirstOrDefault().Key;  //"not sent";
                            FundingRepository.UpdateApplicationFunding(applicationfunding);
                        },
                        rollback: () => FundingRepository.Remove(applicationfunding)
                    );
                    CommandExecutor.Execute(new List<Command>() { AddFundingRequestCommand });
                });

                await EventHubClient.Publish(new ApplicationFundingStatusUpdated
                {
                    EntityType = applicationfunding.EntityType,
                    EntityId = applicationfunding.EntityId,
                    Response = applicationfunding,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                await EventHubClient.Publish(new ApplicationFundingUpdated
                {
                    EntityId = applicationfunding.EntityType,
                    EntityType = applicationfunding.EntityId,
                    ReferenceId = applicationfunding.ReferenceId,
                    OldStatus = oldStatus,
                    NewStatus = applicationfunding.RequestStatus
                });
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateFundingRequest Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service:Funding" + "Exception" + exception.Message);

            }
            Logger.Info("Completed Execution for UpdateFundingRequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "  Service:Funding");
            return applicationfunding;
        }

        public async Task<List<IApplicationFundingRequestAttempts>> GetAttemptData(string referenceId)
        {

            if (string.IsNullOrEmpty(referenceId))
                throw new ArgumentException($"{nameof(referenceId)} is mandatory");
            try
            {
                var fundingAttemptData = await FundingRequestAttemptsRepository.GetAttemptData(referenceId);

                return fundingAttemptData;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAttemptData Date & Time:" + TenantTime.Now + "Service:Funding" + "Exception" + exception.Message);
                throw;
            }
        }

        #endregion PublicMethods

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}