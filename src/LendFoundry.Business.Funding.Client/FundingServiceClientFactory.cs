﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
namespace LendFoundry.Business.Funding.Client
{
    public class FundingServiceClientFactory : IFundingServiceClientFactory
    {
        #region Constructors

        public FundingServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", Endpoint, Port).Uri;
        }

        public FundingServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }


        #endregion Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }

        private Uri Uri { get; }
        #endregion Private Properties

        #region Public Methods

        public IFundingService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("business-Funding");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new FundingService(client);
        }
        #endregion Public Methods
    }
}