﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Funding.Client
{
    public interface IFundingServiceClientFactory
    {
        IFundingService Create(ITokenReader reader);
    }
}