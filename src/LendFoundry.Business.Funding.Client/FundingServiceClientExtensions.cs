﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Business.Funding.Client
{
    public static class FundingServiceClientExtensions
    {
        public static IServiceCollection AddFundingService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IFundingServiceClientFactory>(p => new FundingServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IFundingServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddFundingService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IFundingServiceClientFactory>(p => new FundingServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IFundingServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddFundingService(this IServiceCollection services)
        {
            services.AddSingleton<IFundingServiceClientFactory>(p => new FundingServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IFundingServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}