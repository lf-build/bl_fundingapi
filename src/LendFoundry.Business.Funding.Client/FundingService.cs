﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Text;
using System.Linq;

namespace LendFoundry.Business.Funding.Client
{
    public class FundingService : IFundingService
    {
        #region Constructors

        public FundingService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IApplicationFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/add", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationFunding>(objRequest);
        }

        public async Task<IApplicationFundingRequestAttempts> AddFundingRequestAttempts(string entityType, string entityId, IFundingRequestAttempts request)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/addfundingattempt", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationFundingRequestAttempts>(objRequest);
        }

        public async Task<IEnumerable<IApplicationFunding>> GetFundingRequest(List<string> statuses)
        {
            var objRequest = new RestRequest("status", Method.GET);
            var strBuilder = new StringBuilder();
            AppendStatus(objRequest, statuses);
            return await Client.ExecuteAsync<List<ApplicationFunding>>(objRequest);
        }

        public async Task<IApplicationFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus updateRequest)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/updatefundingstatus", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<ApplicationFunding>(objRequest);
        }

        public async Task<IApplicationFundingRequestAttempts> UpdateAttamptStatus(string referenceId, IUpdateFundingAttamptStatusRequest updateRequest)
        {
            var objRequest = new RestRequest("{referenceid}/updateattamptstatus", Method.POST);
            objRequest.AddUrlSegment("referenceid", referenceId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<ApplicationFundingRequestAttempts>(objRequest);
        }

        public async Task<IApplicationFunding> UpdateFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            var objRequest = new RestRequest("{entitytype}/{entityid}/update", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationFunding>(objRequest);
        }
        public async Task<IApplicationFunding> GetFundingData(string entityType, string entityId)
        {
            var objRequest = new RestRequest("{entityType}/{entityId}", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<ApplicationFunding>(objRequest);
        }
        #endregion Public Methods

        private static void AppendStatus(IRestRequest request, IReadOnlyList<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < statuses.Count; index++)
            {
                var tag = statuses[index];
                url = url + $"/{{name{index}}}";
                request.AddUrlSegment($"name{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<List<IApplicationFundingRequestAttempts>> GetAttemptData(string referenceId)
        {
            var objRequest = new RestRequest("fundingattempt/{referenceId}", Method.GET);
            objRequest.AddUrlSegment("referenceId", referenceId);
            var result = await Client.ExecuteAsync<List<ApplicationFundingRequestAttempts>>(objRequest);
            return new List<IApplicationFundingRequestAttempts>(result);
        }
    }
}
