﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Business.Funding.Api.Controllers
{
    /// <summary>
    /// FundingController 
    /// </summary>
    [Route("/")]
    public class FundingController : ExtendedController
    {
        #region Constructors
        /// <summary>
        ///  constructor
        /// </summary>
        /// <param name="service"></param>

        public FundingController(IFundingService service)
        {
            FundingService = service;
        }

        #endregion Constructors

        #region Private Properties

        private IFundingService FundingService { get; }

        #endregion Private Properties

        #region Public Methods
        
        /// <summary>
        /// AddAttempts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityid"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpPost("{entitytype}/{entityid}/addfundingattempt")]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        //[ProducesResponseType(typeof(IApplicationFundingRequestAttempts), 200)]        
        public async Task<IActionResult> AddAttempts(string entityType, string entityid, [FromBody]FundingRequestAttempts request)
        {
            if (entityid == null)
                throw new ArgumentNullException(nameof(entityid));
            return await ExecuteAsync(async () => Ok(await (FundingService.AddFundingRequestAttempts(entityType, entityid, request))));
        }
        /// <summary>
        /// AddFundingRequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityid"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpPost("/{entitytype}/{entityid}/add")]
        //[ProducesResponseType(typeof(IApplicationFunding), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddFundingRequest(string entityType, string entityid, [FromBody]FundingRequest request)
        {
            if (entityid == null)
                throw new ArgumentNullException(nameof(entityid));
            return await ExecuteAsync(async () => Ok(await (FundingService.AddFundingRequest(entityType, entityid, request))));
        }
        /// <summary>
        /// UpdateFundingRequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityid"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpPost("{entitytype}/{entityid}/update")]
        //[ProducesResponseType(typeof(IApplicationFunding), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFundingRequest(string entityType, string entityid, [FromBody]FundingRequest request)
        {
            if (entityid == null)
                throw new ArgumentNullException(nameof(entityid));
            return await ExecuteAsync(async () => Ok(await (FundingService.UpdateFundingRequest(entityType, entityid, request))));
        }
        
        /// <summary>
        /// GetFundingRequest
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>

        [HttpGet("status/{*statuses}")]
        //[ProducesResponseType(typeof(IEnumerable<IApplicationFunding>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingRequest(string statuses)
        {
            if (string.IsNullOrWhiteSpace(statuses))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(statuses));

            statuses = WebUtility.UrlDecode(statuses);
            var listStatus = SplitStatus(statuses);
            return await ExecuteAsync(async () => Ok(await (FundingService.GetFundingRequest(listStatus))));
        }
        /// <summary>
        /// GetFundingRequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>

        [HttpGet("{entityType}/{entityId}")]
        //[ProducesResponseType(typeof(IApplicationFunding), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingRequest(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (FundingService.GetFundingData(entityType, entityId))));
        }
        
        /// <summary>
        /// UpdateFundingStatus
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityid"></param>
        /// <param name="updateRequest"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/updatefundingstatus")]
        //[ProducesResponseType(typeof(IApplicationFunding), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFundingStatus(string entityType, string entityid, [FromBody]UpdateFundingStatus updateRequest)
        {
            return await ExecuteAsync(async () => Ok(await (FundingService.UpdateFundingStatus(entityType, entityid, updateRequest))));
        }
        /// <summary>
        /// UpdateFundingAttamptStatus
        /// </summary>
        /// <param name="referenceid"></param>
        /// <param name="updateRequest"></param>
        /// <returns></returns>
        [HttpPost("{referenceid}/updateattamptstatus")]
        //[ProducesResponseType(typeof(IApplicationFundingRequestAttempts), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFundingAttamptStatus(string referenceid, [FromBody]UpdateFundingAttamptStatusRequest updateRequest)
        {
            return await ExecuteAsync(async () => Ok(await (FundingService.UpdateAttamptStatus(referenceid, updateRequest))));
        }
        #endregion Public Methods
        /// <summary>
        /// GetFundingAttemptData
        /// </summary>
        /// <param name="referenceId"></param>
        /// <returns></returns>

        [HttpGet("fundingattempt/{referenceId}")]
        //[ProducesResponseType(typeof(List<IApplicationFundingRequestAttempts>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingAttemptData(string referenceId)
        {
            return await ExecuteAsync(async () => Ok(await (FundingService.GetAttemptData(referenceId))));
        }
        private static List<string> SplitStatus(string statuses)
        {
            return string.IsNullOrWhiteSpace(statuses)
                ? new List<string>()
                : statuses.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}