﻿using LendFoundry.Business.Funding.Persistence;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Funding
{
    public interface IFundingRepositoryFactory
    {
        IFundingRepository Create(ITokenReader reader);
    }
}