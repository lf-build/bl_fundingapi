﻿namespace LendFoundry.Business.Funding
{
    public interface IUpdateFundingStatus
    {
        string ReferenceId { get; set; }

        string status { get; set; }
    }
}