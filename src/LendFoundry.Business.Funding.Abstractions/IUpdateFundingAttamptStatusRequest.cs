﻿using System;

namespace LendFoundry.Business.Funding
{
    public interface IUpdateFundingAttamptStatusRequest
    {
        string ReferenceId { get; set; }
        string Status { get; set; }
        DateTimeOffset ResponseDate { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
    }
}