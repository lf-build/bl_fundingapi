﻿namespace LendFoundry.Business.Funding
{
    public class ApplicationFundingAddFail 
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }

        public IApplicationFunding ApplicationFunding { get; set; }

        public string SyndicationName { get; set; }

        public string Response { get; set; }


    }
}
