﻿using System;

namespace LendFoundry.Business.Funding
{
    public interface IFundingRequest
    {
        string ApplicationFundingId { get; set; }
        double AmountFunded { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string LoanApplicationNumber { get; set; }
        DateTimeOffset RequestedDate { get; set; }
        string status { get; set; }
        string ReferenceApplicationFundingId { get; set; }
        string FundingType { get; set; }

        string ReferenceId { get; set; }

    }
}