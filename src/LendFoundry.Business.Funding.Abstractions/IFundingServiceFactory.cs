﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Funding
{
    public interface IFundingServiceFactory
    {
        IFundingService Create(ITokenReader reader, ILogger logger);
    }
}
