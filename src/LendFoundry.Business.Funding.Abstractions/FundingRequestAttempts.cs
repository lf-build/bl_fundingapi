﻿using System;

namespace LendFoundry.Business.Funding
{
    public class FundingRequestAttempts : IFundingRequestAttempts
    {
        public DateTimeOffset AttemptDate { get; set; }
        public string FundingRequestAttemptId { get; set; }
        public string FundingRequestID { get; set; }
        public string LoanNumber { get; set; }
        public string OutboundRef { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnReason { get; set; }
        public DateTimeOffset ReturnDate { get; set; }
        public DateTimeOffset VendorResponseDate { get; set; }
        public string VendorResponseStatus { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string ReferenceId { get; set; }
    }
}