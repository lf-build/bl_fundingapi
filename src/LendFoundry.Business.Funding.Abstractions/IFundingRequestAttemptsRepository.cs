﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Funding.Persistence
{
    public interface IFundingRequestAttemptsRepository : IRepository<IApplicationFundingRequestAttempts>
    {
        Task<IApplicationFundingRequestAttempts> UpdateStatus(string referenceId, IUpdateFundingAttamptStatusRequest updateRequest);

        Task<List<IApplicationFundingRequestAttempts>> GetAttemptData(string referenceId);
    }
}