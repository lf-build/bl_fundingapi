﻿using System;

namespace LendFoundry.Business.Funding
{
    public interface IFundingRequestAttempts
    {
        DateTimeOffset AttemptDate { get; set; }
        string FundingRequestAttemptId { get; set; }
        string FundingRequestID { get; set; }
        string LoanNumber { get; set; }
        string OutboundRef { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
        DateTimeOffset ReturnDate { get; set; }
        DateTimeOffset VendorResponseDate { get; set; }
        string VendorResponseStatus { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string ReferenceId { get; set; }
    }
}