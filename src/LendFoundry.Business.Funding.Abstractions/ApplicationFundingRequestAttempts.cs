﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Funding
{
    public class ApplicationFundingRequestAttempts : Aggregate, IApplicationFundingRequestAttempts
    {
        public ApplicationFundingRequestAttempts() { }
        public ApplicationFundingRequestAttempts(IFundingRequestAttempts request)
        {
            AttemptDate =new TimeBucket(request.AttemptDate);
            FundingRequestID = request.FundingRequestID;
            OutboundRef = request.OutboundRef;
            ReturnCode = request.ReturnCode;
            ReturnDate =new TimeBucket(request.ReturnDate);
            VendorResponseDate =new TimeBucket(request.VendorResponseDate);
            VendorResponseStatus = request.VendorResponseStatus;
            ReferenceId = request.ReferenceId;
            BankAccountNumber = request.BankAccountNumber;
            BankAccountType = request.BankAccountType;
            BankRTN = request.BankRTN;
            BorrowersName = request.BorrowersName;
            ReturnReason = request.ReturnReason;
        }

        public TimeBucket AttemptDate { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FundingRequestAttemptId { get; set; }
        public string FundingRequestID { get; set; }
        public string LoanNumber { get; set; }
        public string OutboundRef { get; set; }
        public string ReturnCode { get; set; }
        public TimeBucket ReturnDate { get; set; }
        public TimeBucket VendorResponseDate { get; set; }
        public string VendorResponseStatus { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string ReferenceId { get; set; }
        public string ReturnReason { get; set; }
    }
}