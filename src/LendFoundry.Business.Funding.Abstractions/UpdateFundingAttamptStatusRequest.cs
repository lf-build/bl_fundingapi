﻿using System;

namespace LendFoundry.Business.Funding
{

    public class UpdateFundingAttamptStatusRequest : IUpdateFundingAttamptStatusRequest
        {
            public string ReferenceId { get; set; }

            public string Status { get; set; }

            public DateTimeOffset ResponseDate { get; set; }

            public string ReturnCode { get; set; }

            public string ReturnReason { get; set; }
    }
  
}
