﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Funding.Persistence
{
    public interface IFundingRepository : IRepository<IApplicationFunding>
    {
        Task<IEnumerable<IApplicationFunding>> GetFundingRequest(IEnumerable<string> statuses);

        Task<IApplicationFunding> UpdateFundingStatus(string EntityType, string LoanNumber,IUpdateFundingStatus updateRequest);

        Task<IApplicationFunding> UpdateApplicationFunding(IApplicationFunding updateRequest);

        Task<IApplicationFunding> GetFundingRequest(string entityType, string entityId);
    }
}