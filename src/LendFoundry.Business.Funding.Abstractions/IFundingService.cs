﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Funding
{
    public interface IFundingService
    {
        Task<IApplicationFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request);

        Task<IApplicationFundingRequestAttempts> AddFundingRequestAttempts(string entityType, string entityId, IFundingRequestAttempts request);

        Task<IEnumerable<IApplicationFunding>> GetFundingRequest(List<string> statuses);

        Task<IApplicationFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus status);

        Task<IApplicationFundingRequestAttempts> UpdateAttamptStatus(string referenceId, IUpdateFundingAttamptStatusRequest updateRequest);

        Task<IApplicationFunding> UpdateFundingRequest(string entityType, string entityId, IFundingRequest request);

        Task<IApplicationFunding> GetFundingData(string entityType, string entityId);

        Task<List<IApplicationFundingRequestAttempts>> GetAttemptData(string referenceId);
    }
}