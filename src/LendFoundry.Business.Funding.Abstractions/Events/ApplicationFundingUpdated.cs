﻿namespace LendFoundry.Business.Funding
{
    public class ApplicationFundingUpdated
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string ReferenceId { get; set; }
        public string OldStatus { get; set; }
        public string NewStatus { get; set; }
    }
}
