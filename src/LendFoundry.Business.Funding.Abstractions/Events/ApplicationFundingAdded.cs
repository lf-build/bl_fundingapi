﻿namespace LendFoundry.Business.Funding
{
    public class ApplicationFundingAdded 
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }

        public IApplicationFunding ApplicationFunding { get; set; }

        public string ReferenceId { get; set; }

    }
}
