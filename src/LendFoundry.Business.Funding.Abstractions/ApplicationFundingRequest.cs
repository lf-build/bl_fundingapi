﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Funding
{
    public class ApplicationFunding : Aggregate, IApplicationFunding
    {
        public ApplicationFunding()
        { }
        public ApplicationFunding(IFundingRequest request)
        {
            AmountFunded = request.AmountFunded;
            BankAccountNumber = request.BankAccountNumber;
            BankAccountType = request.BankAccountType;
            BankRTN = request.BankRTN;
            BorrowersName = request.BorrowersName;
            RequestedDate = new TimeBucket(request.RequestedDate);
            ReferenceApplicationFundingId = request.ReferenceApplicationFundingId;
            FundingType = request.FundingType;
            ReferenceId = request.ReferenceId;
            ReferenceApplicationFundingId = request.ReferenceApplicationFundingId;
            FundingType = request.FundingType;
            RequestStatus = request.status;
        }

        public double AmountFunded { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string LoanApplicationNumber { get; set; }
        public TimeBucket RequestedDate { get; set; }
        public string RequestStatus { get; set; }

        public string ReferenceId { get; set; }

        public string ReferenceApplicationFundingId { get; set; }

        public string FundingType { get; set; }
    }
}