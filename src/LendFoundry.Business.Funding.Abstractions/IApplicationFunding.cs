﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Funding
{
    public interface IApplicationFunding : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        double AmountFunded { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string LoanApplicationNumber { get; set; }
        TimeBucket RequestedDate { get; set; }
        string RequestStatus { get; set; }
        string ReferenceId { get; set; }
        string ReferenceApplicationFundingId { get; set; }
        string FundingType { get; set; }

    }
}