﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Business.Funding
{
    public interface IFundingConfiguration :IDependencyConfiguration
    {
         string ConnectionString { get; set; }

    }
}