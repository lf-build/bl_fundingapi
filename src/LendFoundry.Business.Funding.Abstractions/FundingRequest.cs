﻿using System;

namespace LendFoundry.Business.Funding
{
    public class FundingRequest : IFundingRequest
    {
        public string ApplicationFundingId { get; set; }
        public double AmountFunded { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }        
        public string LoanApplicationNumber { get; set; }
        public DateTimeOffset RequestedDate { get; set; }
        public string status { get; set; }
        public string ReferenceApplicationFundingId { get; set; }
        public string FundingType { get; set; }

        public string ReferenceId { get; set; }
    }
}