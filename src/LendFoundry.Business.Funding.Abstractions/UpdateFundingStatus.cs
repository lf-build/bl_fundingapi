﻿using System;

namespace LendFoundry.Business.Funding
{
    public class UpdateFundingStatus : IUpdateFundingStatus
    {
        public  string ReferenceId { get; set; }
        public string status { get; set; }
    }
}