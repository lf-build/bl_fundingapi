﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Business.Funding
{
    public static class Settings
    {
      public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-Funding";
          
    }
}