﻿using LendFoundry.Business.Funding.Persistence;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Funding
{
    public interface IFundingRequestAttemptsRepositoryFactory
    {
        IFundingRequestAttemptsRepository Create(ITokenReader reader);
    }
}