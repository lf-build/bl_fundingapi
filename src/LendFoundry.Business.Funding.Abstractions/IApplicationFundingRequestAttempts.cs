﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Funding
{
    public interface IApplicationFundingRequestAttempts : IAggregate
    {
        TimeBucket AttemptDate { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        string FundingRequestAttemptId { get; set; }
        string FundingRequestID { get; set; }
        string LoanNumber { get; set; }
        string OutboundRef { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
        TimeBucket ReturnDate { get; set; }
        TimeBucket VendorResponseDate { get; set; }
        string VendorResponseStatus { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string ReferenceId { get; set; }
    }
}