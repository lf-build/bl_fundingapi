using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.Funding {
    public class FundingConfiguration : IFundingConfiguration, IDependencyConfiguration

    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}